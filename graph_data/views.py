# from django.shortcuts import render
from django.core import serializers
from django.views import generic
from .models import Data


class DataListView(generic.ListView):
    model = Data

    def get_queryset(self):
        data = Data.objects.using('datadb').filter(date__gte=20111005).filter(date__lte=20111018)
        return data

    def get_context_data(self, **kwargs):
        context = super(DataListView, self).get_context_data(**kwargs)
        context['data_serialized'] = serializers.serialize('json', self.get_queryset())
        return context
