from django.db import models

# Create your models here.
class Data(models.Model):
    date = models.IntegerField(db_column='Date', blank=True, primary_key=True)  # Field name made lowercase.
    param1 = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    param2 = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)
    param3 = models.DecimalField(blank=True, null=True, max_digits=10, decimal_places=2)

    class Meta:
        managed = False
        db_table = 'data'

    def __str__(self):
        """
        String for representing the Model object
        """
        return ' Bla bla %s ' % (self.date)