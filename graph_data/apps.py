from django.apps import AppConfig


class GraphDataConfig(AppConfig):
    name = 'graph_data'
